import module_1 as mod1

class FirstController:
    @classmethod
    def get_class_name(self):
        return self.__name__

    @classmethod
    def get_module_name(self):
        return mod1.__name__
    pass
