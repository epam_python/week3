import module_2 as mod2

class SecondController:
    @classmethod
    def get_class_name(self):
        return self.__name__

    @classmethod
    def get_module_name(self):
        return mod2.__name__
    pass
