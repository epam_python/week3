import module_1 as mod1


class SecondModel:
    @classmethod
    def get_module_name(self):
        return self.__name__

    def get_second_model_name(self):
        return mod1.__name__

