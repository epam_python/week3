import module_2 as mod2

class FirstModel:
    @classmethod
    def get_module_name(self):
        return self.__name__

    def get_second_model_name(self):
        return mod2.__name__


